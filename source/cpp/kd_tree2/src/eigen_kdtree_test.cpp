#include "eigen_01_point_loading.h"
#include "eigen_kdtree.h"
#include <iostream>
#include <fstream>

using namespace std;

using ContainerType = std::vector<Vector3f, Eigen::aligned_allocator<Vector3f> >;
using TreeNodeType = TreeNode_<ContainerType::iterator>;

int main(int argc, char** argv) {
  // generate 1000 random points
  ContainerType kd_points(1000000);
  for (auto& v: kd_points) {
    v=Vector3f::Random()*100;
  }

  
  cerr << "tree construction " << endl;
  // construct a kd_tree, with leaf size 10
  // the construction reshuffles the items in the container
  TreeNodeType  kd_tree(kd_points.begin(), kd_points.end(), 10);

  cerr << "tree ok" << endl;
  float ball_radius=1;
 
  ContainerType query_points(1000);
  for (auto& v: query_points) {
    v=Vector3f::Random()*100;
  }
  // we search each point in the input set. We need to find a match
  for (auto p: query_points) {
    TreeNodeType::AnswerType neighbors;
    kd_tree.fastSearch(neighbors, p, ball_radius);
    int num_fast=neighbors.size();

    neighbors.clear();
    kd_tree.fullSearch(neighbors, p, ball_radius);
    int num_full=neighbors.size();

    neighbors.clear();
    bruteForceSearch(neighbors,
                     kd_points.begin(),
                     kd_points.end(),
                     p,
                     ball_radius);
    int num_true=neighbors.size();

    neighbors.clear();
    
    Vector3f* match_fast=kd_tree.bestMatchFast(p, ball_radius);
    Vector3f* match_full=kd_tree.bestMatchFull(p, ball_radius);

    cout << "(" << num_fast << "/" << num_full << "/" <<num_true << ")";
    if (match_fast==match_full)
      cout << "FAST Correct" << endl;
    else {
      cout << "FAST Not Correct: ";
      if (! match_fast) {
        cout << " NONE ";
      } else {
        cout << match_fast->transpose() << " ";
      }
      cout << " - ";
      if (! match_full) {
        cout << " NONE ";
      } else {
        cout << match_full->transpose() << " ";
      }
      cout << endl;
        
    }
    if (num_full!=num_true) {
      cout << "Something wrong" << endl;
      exit(0);
    }
  }
  cout << endl;


  
}



